class Bag(object):
    def __init__(self):
        self.items = []

    def add_item(self, item):
        self.items.append(item)

    def remove_item(self, item):
        self.items.remove(item)

    def list_bag(self):
        for item in self.items:
            print(item)


my_bag = Bag()
my_bag.add_item(1)
my_bag.add_item(2)
my_bag.add_item(5)
my_bag.list_bag()
my_bag.remove_item(5)
my_bag.list_bag()
