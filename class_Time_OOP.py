class Time(object):
    def __init__(self, hour=0, minute=0):
        self.hour = hour
        self.minute = minute

    @classmethod
    def from_string(cls, time_str):
        hour, minute = map(int, time_str.split(':'))
        return cls(hour, minute)

    @staticmethod
    def is_valid(time_str):
        hour, minute = map(int, time_str.split(':'))
        return hour <= 23 and minute <= 59


time1 = Time(22, 7)
time2 = Time.from_string('22:07')

print('time1: {}-{}'.format(time1.hour, time1.minute))
print('time2: {}-{}'.format(time2.hour, time2.minute))


is_time = Time.is_valid('22:07')
print(is_time)
